/* global myscada,activeRecipeData,activeRecipeDataRows,activeRecipeRow,activeRecipeIndex,activeRecipeIndexes,activeRecipeFilteredData,activeRecipeFilteredDataRows,activeRecipeFilterHash,activeRecipeFilter1,activeRecipeFilter2,activeRecipeFilter3,activeRecipeFilter4,activeRecipeFilter5,activeRecipeFilter6,activeRecipeFilter7,activeRecipeFilter8,activeRecipeFilter9,knx_startStatus,knx_testFB,knx_startStatus_toPLC,knx_testFB_toPLC,timi,dags,klst */
function init() {
// initialization code

}

function destroy() {
// view hide code

}

function periodic() {
// periodically triggering code
dags=new Date().getTime()/1000;
timi=new Date()/1000;
klst=new Date().getHours();

knx_startStatus_toPLC = knx_startStatus;
knx_testFB_toPLC = knx_testFB;

}

